package at.campus02.entensimulator;

public class NormalFliegen implements Flugart {
    @Override
    public void fliegen(String name) {
        System.out.println(name + " fliegt");
    }
}
