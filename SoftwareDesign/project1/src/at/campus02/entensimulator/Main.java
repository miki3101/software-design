package at.campus02.entensimulator;

public class Main {

    public static void main(String[] args) {

        Ente maxi = new Ente("Maxi", new LangsamFliegen());
        Ente susi = new Ente("Susi", new NormalFliegen());
        Ente frida = new Ente("Frida", new SchnellFliegen());

        maxi.gehen();
        susi.quaken();
        frida.quaken();
        System.out.println("----------------");
        maxi.fliegen();
        susi.fliegen();
        frida.fliegen();
        System.out.println("----------------");
    }
}
