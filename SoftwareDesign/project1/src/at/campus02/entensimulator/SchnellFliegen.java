package at.campus02.entensimulator;

public class SchnellFliegen implements Flugart {

    public void fliegen(String name) {
        System.out.println(name + " fliegt schnell");
    }
}
