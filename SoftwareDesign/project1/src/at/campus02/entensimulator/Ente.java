package at.campus02.entensimulator;

public class Ente {

    public String name;
    public Flugart flugart;

    public Ente(String name, Flugart flugart) {
        this.name = name;
        this.flugart = flugart;
    }

    public void quaken(){
        System.out.println(name + " quakt");
    }

    public void gehen(){
        System.out.println(name + " geht");
    }

    public void fliegen(){
       flugart.fliegen(name);
    }
}
