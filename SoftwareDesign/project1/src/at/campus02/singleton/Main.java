package at.campus02.singleton;


public class Main {

    public static void main(String[] args) {

       printUsername();
       printConnection();
    }

    private static void printConnection(){
        System.out.println("Connection: " + Config.instance().getConnectionString());
    }

    private static void printUsername(){
        System.out.println("Username: " + Config.instance().getUserName());
    }

}
