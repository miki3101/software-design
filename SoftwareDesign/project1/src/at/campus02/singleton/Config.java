package at.campus02.singleton;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Config {

    private static Config myConfig;


    public static Config instance(){

        if(myConfig == null) {
            myConfig = new Config();
        }
        return myConfig;
    }

    private String connectionString;
    private String username;

    public Config() {

        try(FileInputStream stream = new FileInputStream("at/campus02/singleton/Resources/config.properties")){

            System.out.println("reading configs...");
            Properties properties = new Properties();
            properties.load(stream);

            connectionString = properties.getProperty("connectionString", "<defaultConnection>");
            username = properties.getProperty("username", "<default>");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getConnectionString(){
        return connectionString;
    }

    public String getUserName(){
        return username;
    }
}
